package pl.gawronlucas.tshtask1.data

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test

class DateUtilsTest {

    @Test
    @Throws(Exception::class)
    fun unixTimestampToMillisTimestamp_isCorrect() {
        val unixTimestamp = 1506003331 // input
        val millisTimestamp = 1506003331000 // expected

        assertEquals(millisTimestamp, unixTimeToMillisTime(unixTimestamp))
    }

    @Test
    @Throws(Exception::class)
    fun unixTimestampToMillisTimestamp_negativeInputShouldThrow() {
        val input = -1
        try {
            unixTimeToMillisTime(input)
        } catch (e: IllegalArgumentException) {
            return
        }
        fail()
    }

    @Test
    @Throws(Exception::class)
    fun unixTimestampToMillisTimestamp_zeroInputShouldThrow() {
        val input = 0
        try {
            unixTimeToMillisTime(input)
        } catch (e: IllegalArgumentException) {
            return
        }
        fail()
    }
}