package pl.gawronlucas.tshtask1.data

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import pl.gawronlucas.tshtask1.data.mapper.CountryBaseModelMapper
import pl.gawronlucas.tshtask1.data.mapper.CountryDetailsModelMapper
import pl.gawronlucas.tshtask1.data.model.CountryBaseModel
import pl.gawronlucas.tshtask1.data.model.CountryDetailsModel
import pl.gawronlucas.tshtask1.domain.Country
import pl.gawronlucas.tshtask1.domain.CountryBase

/**
 * Created by lucas on 21.09.17.
 */
class MappersTest {

    private val countryBaseModelMapper = CountryBaseModelMapper()
    private val countryDetailsModelMapper = CountryDetailsModelMapper()

    @Test
    @Throws(Exception::class)
    fun countryBaseModelMapper_isCorrect() {
        val baseModel = CountryBaseModel(1, "country1", "url", 1)
        val expectedBase = CountryBase(1, "country1", "url", 1000)
        assertEquals(expectedBase, countryBaseModelMapper.map(baseModel))
    }

    @Test
    @Throws(Exception::class)
    fun countryDetailsModelMapper_isCorrect() {
        val base = CountryBase(1, "country1", "url", 1000)
        val expectedDetails = Country(base, "desc", "seeMoreUrl")

        val detailsModel = CountryDetailsModel(1, "country1", "url", 1, "desc", "seeMoreUrl")
        assertEquals(expectedDetails, countryDetailsModelMapper.map(detailsModel))
    }

    @Test
    @Throws(Exception::class)
    fun countryBaseModelMapper_dateNegativeShouldThrow() {
        val baseModel = CountryBaseModel(1, "country1", "url", -1)
        try {
            countryBaseModelMapper.map(baseModel)
        } catch (e: IllegalArgumentException) {
            return
        }
        fail()
    }

    @Test
    @Throws(Exception::class)
    fun countryDetailsModelMapper_dateNegativeShouldThrow() {
        val detailsModel = CountryDetailsModel(1, "country1", "url", -1, "desc", "seeMoreUrl")
        try {
            countryDetailsModelMapper.map(detailsModel)
        } catch (e: IllegalArgumentException) {
            return
        }
        fail()
    }

}