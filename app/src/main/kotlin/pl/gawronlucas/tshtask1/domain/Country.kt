package pl.gawronlucas.tshtask1.domain

/**
 * Created by lgawron on 09.09.2017.
 */
data class Country(val base: CountryBase, val description: String, val seeMoreUrl: String)