package pl.gawronlucas.tshtask1.domain

import io.reactivex.Single

/**
 * Created by lgawron on 10.09.2017.
 */
interface CountriesRepository {
    fun getCountries(): Single<List<CountryBase>>
    fun getCountry(id: Int): Single<Country>
}