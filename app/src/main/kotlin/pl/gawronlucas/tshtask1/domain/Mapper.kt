package pl.gawronlucas.tshtask1.domain

/**
 * Created by lgawron on 09.09.2017.
 */
interface Mapper<in From, out To> {
    fun map(obj: From): To
}