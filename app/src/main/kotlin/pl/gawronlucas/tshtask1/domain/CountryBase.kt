package pl.gawronlucas.tshtask1.domain

/**
 * Created by lgawron on 09.09.2017.
 */
data class CountryBase(val id: Int, val name: String, val pictureUrl: String, val date: Long)