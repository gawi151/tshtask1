package pl.gawronlucas.tshtask1.util

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by lucas on 12.09.17.
 */
fun Disposable.isNotDisposed() = this.isDisposed.not()

fun Disposable.addTo(compositeDisposable: CompositeDisposable) : Disposable {
    compositeDisposable.add(this)
    return this
}