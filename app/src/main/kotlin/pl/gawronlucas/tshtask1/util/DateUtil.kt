package pl.gawronlucas.tshtask1.util

import java.text.DateFormat
import java.util.*

/**
 * Created by lucas on 12.09.17.
 */
fun Long.toDate(): Date = Date(this)

fun Date.format(dateFormat: DateFormat): String = dateFormat.format(this)