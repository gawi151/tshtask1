package pl.gawronlucas.tshtask1.data.service

import io.reactivex.Single
import pl.gawronlucas.tshtask1.data.model.CountryBaseModel
import pl.gawronlucas.tshtask1.data.model.CountryDetailsModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by lgawron on 10.09.2017.
 */
@Singleton
class RetrofitCountriesService
@Inject constructor() : CountriesService {

    private var service: CountriesService = Retrofit.Builder()
            .baseUrl(CountriesService.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(CountriesService::class.java)

    override fun getCountryList(): Single<List<CountryBaseModel>> = service.getCountryList()

    override fun getCountry(id: Int): Single<CountryDetailsModel> = service.getCountry(id)
}