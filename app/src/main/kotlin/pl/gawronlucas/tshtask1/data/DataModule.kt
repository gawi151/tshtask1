package pl.gawronlucas.tshtask1.data

import dagger.Binds
import dagger.Module
import pl.gawronlucas.tshtask1.data.datasource.CountriesDataSource
import pl.gawronlucas.tshtask1.data.datasource.NetworkCountriesDataSource
import pl.gawronlucas.tshtask1.data.repository.CountriesRepositoryImpl
import pl.gawronlucas.tshtask1.data.service.CountriesService
import pl.gawronlucas.tshtask1.data.service.RetrofitCountriesService
import pl.gawronlucas.tshtask1.domain.CountriesRepository

/**
 * Created by lucas on 20.09.17.
 */
@Module
abstract class DataModule {

    @Binds abstract fun countriesService(countriesService: RetrofitCountriesService): CountriesService
    @Binds abstract fun countreisDataSource(countriesDataSource: NetworkCountriesDataSource): CountriesDataSource
    @Binds abstract fun countriesRepository(repository: CountriesRepositoryImpl): CountriesRepository

}