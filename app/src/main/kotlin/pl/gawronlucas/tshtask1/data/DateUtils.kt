package pl.gawronlucas.tshtask1.data

/**
 * Created by lgawron on 10.09.2017.
 */
@Throws(IllegalArgumentException::class)
fun unixTimeToMillisTime(timestamp: Int): Long  {
    if (timestamp <= 0) throw IllegalArgumentException("Timestamp cannot be zero or negative value.")
    return timestamp.toLong() * 1000
}