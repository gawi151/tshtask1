package pl.gawronlucas.tshtask1.data.repository

import io.reactivex.Single
import pl.gawronlucas.tshtask1.data.datasource.CountriesDataSource
import pl.gawronlucas.tshtask1.data.mapper.CountryBaseModelMapper
import pl.gawronlucas.tshtask1.data.mapper.CountryDetailsModelMapper
import pl.gawronlucas.tshtask1.domain.CountriesRepository
import pl.gawronlucas.tshtask1.domain.Country
import pl.gawronlucas.tshtask1.domain.CountryBase
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by lgawron on 10.09.2017.
 */
@Singleton
class CountriesRepositoryImpl
@Inject constructor(val dataSource: CountriesDataSource,
                    val countryBaseModelMapper: CountryBaseModelMapper,
                    val countryDetailsModelMapper: CountryDetailsModelMapper) : CountriesRepository {
    override fun getCountries(): Single<List<CountryBase>> {
        return dataSource.getCountries().map { list ->
            list.map { model -> countryBaseModelMapper.map(model) }
        }
    }

    override fun getCountry(id: Int): Single<Country> =
            dataSource.getCountry(id).map { countryDetailsModelMapper.map(it) }
}