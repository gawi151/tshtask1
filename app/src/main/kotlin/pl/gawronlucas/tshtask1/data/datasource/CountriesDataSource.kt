package pl.gawronlucas.tshtask1.data.datasource

import io.reactivex.Single
import pl.gawronlucas.tshtask1.data.model.CountryBaseModel
import pl.gawronlucas.tshtask1.data.model.CountryDetailsModel

/**
 * Created by lgawron on 10.09.2017.
 */
interface CountriesDataSource {
    fun getCountries(): Single<List<CountryBaseModel>>
    fun getCountry(id: Int): Single<CountryDetailsModel>
}