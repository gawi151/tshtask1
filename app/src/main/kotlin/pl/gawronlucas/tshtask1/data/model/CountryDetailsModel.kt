package pl.gawronlucas.tshtask1.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by lgawron on 09.09.2017.
 */
data class CountryDetailsModel(@SerializedName("id") @Expose val id: Int,
                               @SerializedName("name") @Expose val name: String,
                               @SerializedName("picture_url") @Expose val pictureUrl: String,
                               @SerializedName("date") @Expose val date: Int,
                               @SerializedName("description") @Expose val description: String,
                               @SerializedName("see_more_url") @Expose val seeMoreUrl: String)