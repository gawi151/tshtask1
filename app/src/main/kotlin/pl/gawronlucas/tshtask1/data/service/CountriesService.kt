package pl.gawronlucas.tshtask1.data.service

import io.reactivex.Single
import pl.gawronlucas.tshtask1.data.model.CountryBaseModel
import pl.gawronlucas.tshtask1.data.model.CountryDetailsModel
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by lgawron on 10.09.2017.
 */
interface CountriesService {

    companion object {
        const val BASE_URL = "https://serene-mountain-2455.herokuapp.com/"
    }

    @GET("countries")
    fun getCountryList(): Single<List<CountryBaseModel>>

    @GET("countries/{id}")
    fun getCountry(@Path("id") id: Int): Single<CountryDetailsModel>
}