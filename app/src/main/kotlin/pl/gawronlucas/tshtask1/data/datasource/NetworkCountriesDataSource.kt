package pl.gawronlucas.tshtask1.data.datasource

import io.reactivex.Single
import pl.gawronlucas.tshtask1.data.model.CountryBaseModel
import pl.gawronlucas.tshtask1.data.model.CountryDetailsModel
import pl.gawronlucas.tshtask1.data.service.CountriesService
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by lgawron on 10.09.2017.
 */
@Singleton
class NetworkCountriesDataSource
@Inject constructor(val service: CountriesService) : CountriesDataSource {
    override fun getCountries(): Single<List<CountryBaseModel>> = service.getCountryList()
    override fun getCountry(id: Int): Single<CountryDetailsModel> = service.getCountry(id)
}