package pl.gawronlucas.tshtask1.data.mapper

import pl.gawronlucas.tshtask1.data.model.CountryDetailsModel
import pl.gawronlucas.tshtask1.data.unixTimeToMillisTime
import pl.gawronlucas.tshtask1.domain.Country
import pl.gawronlucas.tshtask1.domain.CountryBase
import pl.gawronlucas.tshtask1.domain.Mapper
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by lgawron on 10.09.2017.
 */
@Singleton
class CountryDetailsModelMapper @Inject constructor() : Mapper<CountryDetailsModel, Country> {
    override fun map(obj: CountryDetailsModel): Country {
        val countryBase = CountryBase(obj.id, obj.name,
                obj.pictureUrl, unixTimeToMillisTime(obj.date))
        return Country(countryBase, obj.description, obj.seeMoreUrl)
    }
}