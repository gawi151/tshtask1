package pl.gawronlucas.tshtask1.data.mapper

import pl.gawronlucas.tshtask1.data.model.CountryBaseModel
import pl.gawronlucas.tshtask1.data.unixTimeToMillisTime
import pl.gawronlucas.tshtask1.domain.CountryBase
import pl.gawronlucas.tshtask1.domain.Mapper
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by lgawron on 10.09.2017.
 */
@Singleton
class CountryBaseModelMapper @Inject constructor() : Mapper<CountryBaseModel, CountryBase> {
    override fun map(obj: CountryBaseModel): CountryBase =
            CountryBase(obj.id, obj.name, obj.pictureUrl, unixTimeToMillisTime(obj.date))
}