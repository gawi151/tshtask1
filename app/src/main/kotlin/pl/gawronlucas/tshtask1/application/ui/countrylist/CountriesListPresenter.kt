package pl.gawronlucas.tshtask1.application.ui.countrylist

import pl.gawronlucas.tshtask1.domain.CountryBase
import pl.gawronlucas.tshtask1.application.ui.Presenter

/**
 * Created by lgawron on 09.09.2017.
 */
interface CountriesListPresenter : Presenter<CountriesListView> {
    fun getCountries()
    fun countrySelected(countryBase: CountryBase)
}