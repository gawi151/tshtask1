package pl.gawronlucas.tshtask1.application

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.gawronlucas.tshtask1.application.ui.countrydetails.CountryDetailsActivity
import pl.gawronlucas.tshtask1.application.ui.countrydetails.CountryDetailsActivityModule
import pl.gawronlucas.tshtask1.application.ui.countrylist.CountriesListActivity
import pl.gawronlucas.tshtask1.application.ui.countrylist.CountriesListActivityModule
import pl.gawronlucas.tshtask1.application.ui.di.ActivityScope


/**
 * Created by lucas on 20.09.17.
 */
@Module
abstract class AndroidBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(CountriesListActivityModule::class))
    abstract fun countriesListActivity(): CountriesListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(CountryDetailsActivityModule::class))
    abstract fun countryDetailsActivity(): CountryDetailsActivity
}