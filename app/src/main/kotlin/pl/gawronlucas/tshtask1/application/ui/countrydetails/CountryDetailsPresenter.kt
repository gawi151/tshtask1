package pl.gawronlucas.tshtask1.application.ui.countrydetails

import pl.gawronlucas.tshtask1.application.ui.Presenter

/**
 * Created by lucas on 12.09.17.
 */
interface CountryDetailsPresenter : Presenter<CountryDetailsView> {
    fun getCountry(id: Int)
    fun getSeeMore()
}