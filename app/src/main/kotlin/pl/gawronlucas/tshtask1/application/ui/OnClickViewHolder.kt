package pl.gawronlucas.tshtask1.application.ui

/**
 * Created by lucas on 12.09.17.
 */
interface OnClickViewHolder {
    fun onClick(event: (position: Int) -> Unit)
}