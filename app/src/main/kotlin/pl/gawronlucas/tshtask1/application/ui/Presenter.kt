package pl.gawronlucas.tshtask1.application.ui

/**
 * Created by lgawron on 09.09.2017.
 */
interface Presenter<in V : View> {
    fun attachView(view: V)
    fun detachView()
}