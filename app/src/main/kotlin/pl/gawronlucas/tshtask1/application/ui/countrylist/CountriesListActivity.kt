package pl.gawronlucas.tshtask1.application.ui.countrylist

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_countries_list.*
import pl.gawronlucas.tshtask1.R
import pl.gawronlucas.tshtask1.application.ui.countrydetails.CountryDetailsActivity
import pl.gawronlucas.tshtask1.application.ui.util.hide
import pl.gawronlucas.tshtask1.application.ui.util.show
import pl.gawronlucas.tshtask1.domain.CountryBase
import javax.inject.Inject

class CountriesListActivity : DaggerAppCompatActivity(), CountriesListView {

    @Inject lateinit var presenter: CountriesListPresenter
    @Inject lateinit var countryListAdapter: CountriesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_countries_list)
        setSupportActionBar(toolbar)
        countriesList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        countryListAdapter.onItemClick { presenter.countrySelected(it) }
        countriesList.adapter = countryListAdapter

        tryAgainBtn.setOnClickListener { tryAgain() }

        presenter.attachView(this)
        presenter.getCountries()
    }

    override fun onDestroy() {
        presenter.detachView()
        countryListAdapter.clearItemClickListener()
        super.onDestroy()
    }

    // View methods

    override fun showLoading() {
        progress.show()
    }

    override fun hideLoading() {
        progress.hide()
    }

    override fun showError() {
        error.show()
    }

    override fun hideError() {
        error.hide()
    }

    override fun tryAgain() {
        presenter.getCountries()
    }

    override fun showCountries(countries: List<CountryBase>) {
        countriesList.show()
        countryListAdapter.updateCountries(countries)
    }

    override fun showCountryDetails(countryBase: CountryBase) {
        CountryDetailsActivity.start(this, countryBase.id, countryBase.name)
    }
}
