package pl.gawronlucas.tshtask1.application.ui.countrydetails

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pl.gawronlucas.tshtask1.domain.CountriesRepository
import pl.gawronlucas.tshtask1.domain.Country
import pl.gawronlucas.tshtask1.util.addTo
import pl.gawronlucas.tshtask1.util.isNotDisposed
import javax.inject.Inject

/**
 * Created by lucas on 12.09.17.
 */
class CountryDetailsPresenterImpl @Inject constructor(val countriesRepository: CountriesRepository) : CountryDetailsPresenter {

    private val NO_VIEW = VoidCountryDetailsView()

    private var view: CountryDetailsView = NO_VIEW

    private val disposables = CompositeDisposable()

    private var viewState: ViewState? = null

    override fun attachView(view: CountryDetailsView) {
        this.view = view
    }

    override fun detachView() {
        if (disposables.isNotDisposed()) disposables.dispose()
        this.view = NO_VIEW
    }

    override fun getCountry(id: Int) {
        view.hideError()
        view.hideContent()
        view.showLoading()

        // below code can be transformed to some sort of work unit or use case to encapsulate user intent
        countriesRepository.getCountry(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    updateViewState(it)
                    view.hideLoading()
                    view.showCountry(it)
                    view.showContent()
                }, {
                    view.hideLoading()
                    view.hideContent()
                    view.showError()
                })
                .addTo(disposables)
    }

    override fun getSeeMore() {
        viewState?.let { view.showSeeMore(it.country.seeMoreUrl) }
    }

    private fun updateViewState(country: Country) {
        this.viewState = ViewState(country)
    }

    inner class ViewState(val country: Country)
}