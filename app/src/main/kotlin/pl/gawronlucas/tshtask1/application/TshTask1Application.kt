package pl.gawronlucas.tshtask1.application

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


/**
 * Created by lucas on 20.09.17.
 */
class TshTask1Application : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        // fabric/loggers etc here
    }


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
            DaggerAppComponent.builder().create(this)

}