package pl.gawronlucas.tshtask1.application.ui.countrydetails

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by lucas on 20.09.17.
 */
@Subcomponent(modules = arrayOf(CountryDetailsActivityModule::class))
abstract class CounryDetialsActivityComponent : AndroidInjector<CountryDetailsActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CountryDetailsActivity>()
}


@Module
abstract class CountryDetailsActivityModule {
    @Binds abstract fun presenter(presenter: CountryDetailsPresenterImpl): CountryDetailsPresenter
}