package pl.gawronlucas.tshtask1.application.ui.countrylist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.gawronlucas.tshtask1.R
import pl.gawronlucas.tshtask1.domain.CountryBase
import javax.inject.Inject

/**
 * Created by lgawron on 10.09.2017.
 */
class CountriesListAdapter @Inject constructor() : RecyclerView.Adapter<CountryViewHolder>() {

    private val countryList: MutableList<CountryBase> = mutableListOf()

    private val NO_CLICK_EVENT: (CountryBase) -> Unit = {}

    private var clickEvent: (CountryBase) -> Unit = NO_CLICK_EVENT

    fun updateCountries(list: List<CountryBase>) {
        countryList.clear()
        countryList.addAll(list)
        notifyDataSetChanged()
    }

    fun onItemClick(click: (item: CountryBase) -> Unit) {
        clickEvent = click
    }

    fun clearItemClickListener() {
        clickEvent = NO_CLICK_EVENT
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.onBind(countryList[position])
        holder.tryAttachClickListener()
    }

    private fun isClickListenerAttached(): Boolean = clickEvent != NO_CLICK_EVENT

    private fun CountryViewHolder.tryAttachClickListener() {
        if (isClickListenerAttached()) {
            this.onClick { clickPosition -> clickEvent(countryList[clickPosition]) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder =
            when (viewType) {
                CountryViewHolder.Type.SIMPLE.ID -> {
                    val view = LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_simple_country, parent, false)
                    SimpleCountryViewHolder(view)
                }
                else -> throw IllegalArgumentException("Unknown view holder type id=$viewType")
            }

    override fun getItemViewType(position: Int): Int = CountryViewHolder.Type.SIMPLE.ID

    override fun getItemCount(): Int = countryList.count()
}