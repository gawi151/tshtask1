package pl.gawronlucas.tshtask1.application.ui

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by lgawron on 10.09.2017.
 */
abstract class CommonViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView),
        OnBindViewHolder<T>, OnRecycleViewModel, ViewTypeHolder, OnClickViewHolder