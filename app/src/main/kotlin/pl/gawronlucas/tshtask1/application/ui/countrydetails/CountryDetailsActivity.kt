package pl.gawronlucas.tshtask1.application.ui.countrydetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_country_details.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.startActivity
import pl.gawronlucas.tshtask1.R
import pl.gawronlucas.tshtask1.application.ui.util.hide
import pl.gawronlucas.tshtask1.application.ui.util.show
import pl.gawronlucas.tshtask1.domain.Country
import pl.gawronlucas.tshtask1.util.format
import pl.gawronlucas.tshtask1.util.toDate
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CountryDetailsActivity : DaggerAppCompatActivity(), CountryDetailsView {

    @Inject lateinit var presenter: CountryDetailsPresenter

    private val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM", Locale.getDefault())

    companion object {
        private const val COUNTRY_ID = "countryIdExtra"
        private const val COUNTRY_NAME = "countryNameExtra"

        fun start(context: Context, id: Int, name: String) {
            context.startActivity<CountryDetailsActivity>(COUNTRY_ID to id, COUNTRY_NAME to name)
        }

        private fun hasRequiredExtras(intent: Intent): Boolean = intent.hasExtra(COUNTRY_ID)
                && intent.hasExtra(COUNTRY_NAME)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_details)

        if (!hasRequiredExtras(intent)) {
            Log.e(this.javaClass.simpleName, "Required params are not provided. " +
                    "Use proper ${this.javaClass.simpleName}.start(...) method to open this activity. Displaying error.")
            finish()
        }

        onCreateIntern()
    }

    private fun onCreateIntern() {
        setupActionBar()
        toolbar.title = intent.getStringExtra(COUNTRY_NAME)
        presenter.attachView(this)
        presenter.getCountry(intent.getIntExtra(COUNTRY_ID, -1))
        seeMoreBtn.setOnClickListener { seeMoreClicked() }
        tryAgainBtn.setOnClickListener { tryAgain() }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showLoading() {
        progress.show()
    }

    override fun hideLoading() {
        progress.hide()
    }

    override fun showError() {
        error.show()
    }

    override fun hideError() {
        error.hide()
    }

    override fun tryAgain() {
        presenter.getCountry(intent.getIntExtra(COUNTRY_ID, -1))
    }

    override fun showContent() {
        countryDescription.show()
        countryImage.show()
        countryVisitDate.show()
        seeMoreBtn.show()
    }

    override fun hideContent() {
        countryDescription.hide()
        countryImage.hide()
        countryVisitDate.hide()
        seeMoreBtn.hide()
    }

    override fun seeMoreClicked() {
        presenter.getSeeMore()
    }

    override fun showSeeMore(url: String) {
        browse(url)
    }

    override fun showCountry(country: Country) {
        downloadImage(country.base.pictureUrl)
        countryDescription.text = country.description
        countryVisitDate.text = country.base.date.toDate().format(dateFormat)
    }

    private fun downloadImage(imageUrl: String) {
        Glide.with(this)
                .load(imageUrl)
                .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                .apply(RequestOptions.centerCropTransform())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                .into(countryImage)
    }
}
