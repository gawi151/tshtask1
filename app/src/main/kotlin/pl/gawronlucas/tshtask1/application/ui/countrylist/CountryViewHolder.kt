package pl.gawronlucas.tshtask1.application.ui.countrylist

import pl.gawronlucas.tshtask1.domain.CountryBase
import pl.gawronlucas.tshtask1.application.ui.CommonViewHolder

/**
 * Created by lgawron on 10.09.2017.
 */
abstract class CountryViewHolder(itemView: android.view.View) : CommonViewHolder<CountryBase>(itemView) {
    enum class Type(val ID: Int) {
        SIMPLE(1),
        EMPTY(2)
    }

    override fun onClick(event: (position: Int) -> Unit) {
        itemView.setOnClickListener { event(adapterPosition) }
    }
}