package pl.gawronlucas.tshtask1.application.ui.di

import javax.inject.Scope

/**
 * Created by lucas on 20.09.17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope