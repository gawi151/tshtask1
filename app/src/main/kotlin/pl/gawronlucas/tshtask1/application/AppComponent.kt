package pl.gawronlucas.tshtask1.application

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import pl.gawronlucas.tshtask1.data.DataModule
import javax.inject.Singleton

/**
 * Created by lucas on 20.09.17.
 */
@Singleton
@Component(modules = arrayOf(DataModule::class, AndroidSupportInjectionModule::class, AndroidBindingModule::class))
interface AppComponent : AndroidInjector<TshTask1Application> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TshTask1Application>()
}