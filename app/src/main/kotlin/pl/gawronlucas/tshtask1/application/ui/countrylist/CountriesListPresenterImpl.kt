package pl.gawronlucas.tshtask1.application.ui.countrylist

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pl.gawronlucas.tshtask1.domain.CountriesRepository
import pl.gawronlucas.tshtask1.domain.CountryBase
import pl.gawronlucas.tshtask1.util.addTo
import javax.inject.Inject

/**
 * Created by lgawron on 10.09.2017.
 */
class CountriesListPresenterImpl @Inject constructor(val countriesRepository: CountriesRepository) : CountriesListPresenter {

    private val NO_VIEW = VoidCountriesListView()

    private var view: CountriesListView = NO_VIEW

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun attachView(view: CountriesListView) {
        this.view = view
    }

    override fun detachView() {
        if (!disposables.isDisposed) disposables.clear()
        view = NO_VIEW
    }

    override fun getCountries() {
        view.hideError()
        view.showLoading()
        countriesRepository.getCountries()
                .map { it.sortedBy { it.date } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list ->
                    view.hideLoading()
                    view.showCountries(list)
                }, {
                    view.hideLoading()
                    view.showError()
                    // fixme use timber
                    Log.e("CountriesPresenter", "Error while getting countries list.", it)
                })
                .addTo(disposables)
    }

    override fun countrySelected(countryBase: CountryBase) {
        view.showCountryDetails(countryBase)
    }
}