package pl.gawronlucas.tshtask1.application.ui.countrydetails

import pl.gawronlucas.tshtask1.domain.Country

/**
 * Created by lucas on 12.09.17.
 */
class VoidCountryDetailsView : CountryDetailsView {
    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showError() {
    }

    override fun hideError() {
    }

    override fun showCountry(country: Country) {
    }

    override fun showSeeMore(url: String) {
    }

    override fun seeMoreClicked() {
    }

    override fun showContent() {
    }

    override fun hideContent() {
    }

    override fun tryAgain() {
    }
}