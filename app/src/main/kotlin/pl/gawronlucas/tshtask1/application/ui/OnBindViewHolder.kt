package pl.gawronlucas.tshtask1.application.ui

/**
 * Created by lgawron on 10.09.2017.
 */
interface OnBindViewHolder<in T> {
    fun onBind(model: T)
}