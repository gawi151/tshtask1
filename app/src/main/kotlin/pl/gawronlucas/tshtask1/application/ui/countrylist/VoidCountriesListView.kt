package pl.gawronlucas.tshtask1.application.ui.countrylist

import pl.gawronlucas.tshtask1.domain.CountryBase

/**
 * Created by lucas on 12.09.17.
 */
class VoidCountriesListView : CountriesListView {
    override fun showError() {
    }

    override fun showCountries(countries: List<CountryBase>) {
    }

    override fun showCountryDetails(countryBase: CountryBase) {
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun hideError() {
    }

    override fun tryAgain() {
    }
}