package pl.gawronlucas.tshtask1.application.ui.countrylist

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by lucas on 20.09.17.
 */
@Subcomponent(modules = arrayOf(CountriesListActivityModule::class))
abstract class CountriesListActivityComponent : AndroidInjector<CountriesListActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CountriesListActivity>()
}

@Module
abstract class CountriesListActivityModule {

    @Binds
    abstract fun presenter(presenter: CountriesListPresenterImpl): CountriesListPresenter
}