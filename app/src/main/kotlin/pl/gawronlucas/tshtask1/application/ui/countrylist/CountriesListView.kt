package pl.gawronlucas.tshtask1.application.ui.countrylist

import pl.gawronlucas.tshtask1.domain.CountryBase
import pl.gawronlucas.tshtask1.application.ui.View

/**
 * Created by lgawron on 09.09.2017.
 */
interface CountriesListView : View {
    fun showError()
    fun hideError()
    fun tryAgain()
    fun showLoading()
    fun hideLoading()
    fun showCountries(countries: List<CountryBase>)
    fun showCountryDetails(countryBase: CountryBase)
}