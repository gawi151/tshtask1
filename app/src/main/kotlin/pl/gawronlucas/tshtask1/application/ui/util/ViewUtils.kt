package pl.gawronlucas.tshtask1.application.ui.util

import android.view.View

/**
 * Created by lgawron on 10.09.2017.
 */

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

var View.visible: Boolean
    get() = isVisible()
    set(value) {
        if (value) visible() else gone()
    }

fun View.isVisible(): Boolean = this.visibility == View.VISIBLE

fun View.isGone(): Boolean = this.visibility == View.GONE

fun View.isInvisible(): Boolean = this.visibility == View.INVISIBLE