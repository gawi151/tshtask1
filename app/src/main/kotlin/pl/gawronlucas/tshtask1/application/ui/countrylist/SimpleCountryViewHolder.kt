package pl.gawronlucas.tshtask1.application.ui.countrylist

import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.item_simple_country.view.*
import pl.gawronlucas.tshtask1.R
import pl.gawronlucas.tshtask1.domain.CountryBase
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by lgawron on 10.09.2017.
 */
class SimpleCountryViewHolder(itemView: android.view.View) : CountryViewHolder(itemView) {

    private val image: ImageView = itemView.countryImageView
    private val name: TextView = itemView.countryNameView
    private val date: TextView = itemView.countryVisitDateView
    private val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM", Locale.getDefault())

    private var imageTarget: Target<Drawable>? = null

    override fun getType(): Int = Type.SIMPLE.ID

    override fun onRecycle() {
        imageTarget?.let { Glide.with(itemView).clear(it) }
    }

    override fun onBind(model: CountryBase) {
        name.text = model.name
        date.text = dateFormat.format(Date(model.date))
        imageTarget = Glide.with(itemView)
                .load(model.pictureUrl)
                .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                .apply(RequestOptions.centerCropTransform())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                .into(image)
    }
}