package pl.gawronlucas.tshtask1.application.ui.countrydetails

import pl.gawronlucas.tshtask1.domain.Country
import pl.gawronlucas.tshtask1.application.ui.View

/**
 * Created by lucas on 12.09.17.
 */
interface CountryDetailsView : View {
    fun showLoading()
    fun hideLoading()
    fun showError()
    fun hideError()
    fun tryAgain()
    fun showContent()
    fun hideContent()
    fun showCountry(country: Country)
    fun seeMoreClicked()
    fun showSeeMore(url: String)
}