package pl.gawronlucas.tshtask1.application.ui.countrylist

import android.view.View
import pl.gawronlucas.tshtask1.domain.CountryBase

/**
 * Created by lgawron on 10.09.2017.
 */
class EmptyCountryListViewHolder(view: View) : CountryViewHolder(view) {

    override fun getType(): Int = CountryViewHolder.Type.EMPTY.ID

    override fun onRecycle() {
    }

    override fun onBind(model: CountryBase) {
    }
}